(require 'cl)
;; This is my personal Emacs config file. It's continually evolving as I learn new and interesting ways
;; of integrating emacs into my workflow.

(setq inhibit-startup-message t)
;;(desktop-save-mode 1)

;; package manager stuff
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '(  "elpa" . "https://elpa.gnu.org/packages/") t)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
  (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/") t)
  )

(defvar jaybill-packages
  '(js2-mode js2-refactor tern tern-auto-complete popwin coffee-mode auto-complete cedet sr-speedbar yaml-mode web-beautify sass-mode scss-mode less-css-mode go-mode go-autocomplete ispell web-mode aggressive-indent php-mode auctex project-explorer flycheck neotree wakatime-mode jsx-mode flymake-json nvm)
  "A list of packages to ensure are installed at launch.")

(defun jaybill-packages-installed-p ()
  (loop for p in jaybill-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

(unless (jaybill-packages-installed-p)
  ;; check for new packages (package versions)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ;; install the missing packages
  (dolist (p jaybill-packages)
    (when (not (package-installed-p p))
      (package-install p))))

(provide 'jaybill-packages)

    (setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))

;;(set-face-attribute 'default t :font "Inconsolata-14" )
(add-to-list 'default-frame-alist '(font . "Inconsolata-14" ))
(require 'popwin)
    
;; recent files
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

;; Spell checka
(global-set-key "\C-c\ \C-c" 'ispell)

(setenv "PATH" (concat (getenv "PATH") ":/home/jaybill/.nvm/versions/node/v4.3.2/bin"))
(setq exec-path (append exec-path '("/home/jaybill/.nvm/versions/node/v4.3.2/bin")))

(global-set-key (kbd "C-c j v") 'flymake-json-load)
  
(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(javascript-jshint)))

(flycheck-add-mode 'javascript-eslint 'web-mode)

(setq-default flycheck-temp-prefix ".flycheck")

(setq-default flycheck-disabled-checkers
              (append flycheck-disabled-checkers
                      '(json-jsonlist)))



;; Auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

;; Pee Ache Pee
(require 'php-mode)

;; Go stuff
(require 'go-mode-autoloads)
(require 'go-autocomplete)
(add-hook 'before-save-hook 'gofmt-before-save)

;; Neotree
(require 'popwin)
(require 'neotree)
(global-set-key "\C-c\ \C-t" 'neotree-toggle)

(when neo-persist-show
    (add-hook 'popwin:before-popup-hook
              (lambda () (setq neo-persist-show nil)))
    (add-hook 'popwin:after-popup-hook
              (lambda () (setq neo-persist-show t))))



;; IDO
(require 'ido)
(ido-mode t)

;; Web stuff
(require 'web-mode)
(require 'coffee-mode)
(require 'yaml-mode)
(require 'sass-mode)
(require 'scss-mode)
(setq coffee-tab-width 2)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ctp\\'" . php-mode))
(add-to-list 'auto-mode-alist '("\\.[gj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.handlebars\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.hbs\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.layout\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(add-hook 'js-mode-hook 'js2-minor-mode)

(setq js2-strict-trailing-comma-warning nil)

;; x stuff
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fringe-mode 4)

(eval-after-load 'web-mode
  '(define-key web-mode-map (kbd "C-c b") 'web-mode-buffer-indent))

;; LaTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq tab-stop-list (number-sequence 2 200 2))
(setq indent-line-function 'insert-tab)
;; automatically clean up bad whitespace
(setq whitespace-action '(auto-cleanup))
;; only show bad whitespace
(setq whitespace-style '(trailing space-before-tab indentation empty space-after-tab))

;;(global-aggressive-indent-mode)

;;(load "~/.emacs.d/jsfmt")
;;(add-hook 'before-save-hook 'jsfmt-before-save)

;;WakkaTime

(setq wakatime-api-key "0f968b1f-aa7c-4a1e-8550-515b2ece2026")
(setq wakatime-cli-path "/usr/local/bin/wakatime")
(global-wakatime-mode)

;; go-mode stuff
;; Needs godef in path - code.google.com/p/rog-go/exp/cmd/godef
;;(require 'go-mode-load)
;;(require 'go-autocomplete)
;;(add-hook 'before-save-hook 'gofmt-before-save)

;; I hate tabs!
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq indent-line-function 'insert-tab)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#839496" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#002b36"))
 '(custom-enabled-themes (quote (sanityinc-solarized-dark)))
 '(custom-safe-themes
   (quote
    ("4cf3221feff536e2b3385209e9b9dc4c2e0818a69a1cdb4b522756bcdf4e00a4" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default)))
 '(fci-rule-color "#073642")
 '(flycheck-eslintrc ".eslintrc.json")
 '(flycheck-javascript-eslint-executable "/home/jaybill/.nvm/versions/node/v4.3.2/bin/eslint")
 '(flycheck-temp-prefix ".flycheck")
 '(global-flycheck-mode t)
 '(js2-basic-offset 2)
 '(standard-indent 2)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#cb4b16")
     (60 . "#b58900")
     (80 . "#859900")
     (100 . "#2aa198")
     (120 . "#268bd2")
     (140 . "#d33682")
     (160 . "#6c71c4")
     (180 . "#dc322f")
     (200 . "#cb4b16")
     (220 . "#b58900")
     (240 . "#859900")
     (260 . "#2aa198")
     (280 . "#268bd2")
     (300 . "#d33682")
     (320 . "#6c71c4")
     (340 . "#dc322f")
     (360 . "#cb4b16"))))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'downcase-region 'disabled nil)
