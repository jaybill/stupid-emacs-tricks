Emacs 24
Personal Cheat Sheet

Misc

C-x C-r         Open Recent

Movement

C-v     Page up
M-v     Page down
C-a     Start of line
C-e     End of line
C-x o   Switch windows
C-M-a   go to start of function
C-M-e   go to end of function

Web Beautify

C-c b   Beautify js/css/html

Speedbar

=       expand
-       close
C-x C-t toggle speedbar

Cut/Paste

C-a C-k C-k C-y C-y     Duplicate Line

Golang

C-c C-d         godef describe
C-c C-j         godef jump
